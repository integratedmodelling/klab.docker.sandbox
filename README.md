Not a lot of time to write this up this morning but here it goes.

This will generate a docker node stack with a development hub.  The hub provides the authentication and networking needed for the node to startup.

The hub is located at http://localhost:8283/hub . To log in the system account is:
user: system
pass: password

You can than navigate to your profile, download certificate and generate an engine certificate for you user.  After this you can place the certificate in  the klab control center and start an engine.  On startup the engine will connect to hub, recieve the url of the node and should connect with the node and list its detail.

I will provide better details, screenshots etc.  But for now this is wher I got to.